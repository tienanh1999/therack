<?php

namespace App\Http\Controllers;
use Excel;
use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Http\Request;

class ExportController extends Controller implements FromCollection, WithHeadings
{
    use Exportable;
    public function collection()
    {
        $orders = Order::all();
        foreach ($orders as $row) {
            $order[] = array(
                '0' => $row->id,
                '1' => $row->name,
                '2' => $row->country,
                '3' => $row->city,
                '4' => $row->address,
                '5' => $row->zipcode,
                '6' => $row->cart,
                '7' => $row->payment_id,
                '8' => $row->created_at,
                '9' => $row->updated_at,
            );
        }

        return (collect($order));
    }
    public function headings(): array
    {
        return [
            'id',
            'Tên',
            'Quốc gia',
            'Thành phố',
            'Địa chỉ',
            'Zipcode',
            'Card id',
            'Id Pay',
            'Ngày tạo',
            'Ngày sửa',
        ];
    }
    public function export(){
        return Excel::download(new $this  , 'orders.xlsx');
    }
}
